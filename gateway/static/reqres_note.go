package static

import (
	"gateway/internal/entities/noteEntity"
)

// swagger:route POST /note Notes NewNoteRequest
// Создание заметки.
// responses:
//  200: NewNoteResponse

// swagger:parameters NewNoteRequest
type NewNoteRequest struct {
	// in:body
	// required: true
	//example: {"note_name":"testName","description":"testDesc","user_id": 1}
	Body noteEntity.Note
}

// swagger:response NewNoteResponse
type NewNoteResponse struct {
	// in:body
	Body string
}

// swagger:route DELETE /note/{id} Notes DeleteNoteRequest
// Удаление заметки.
// responses:
//   200: DeleteNoteResponse
//   400: description: Note ID is required
//   500: description: Failed to delete user.

// swagger:parameters DeleteNoteRequest
type DeleteNoteRequest struct {
	//in:path
	// required: true
	Body string `json:"id"`
}

// swagger:response DeleteNoteResponse
type DeleteNoteResponse struct {
	// in:body
	Body string
}
