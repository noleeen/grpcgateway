// Package static User's notes API.
//
//	 Шапка ты где?!
//
//		Version: 1.0.0
//		Schemes: http, https
//		BasePath: /
//
//		Consumes:
//		- application/json
//	 - multipart/form-data
//
//		Produces:
//		- application/json
//
// swagger:meta
package static

//go:generate swagger generate spec -o ./swagger.json --scan-models
