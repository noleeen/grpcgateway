package static

import (
	"gateway/internal/entities/noteEntity"
	"gateway/internal/entities/userEntity"
)

// swagger:route POST /user Users NewUserRequest
// Создание пользователя.
// responses:
//  200: NewUserResponse

// swagger:parameters NewUserRequest
type NewUserRequest struct {
	// in:body
	// required: true
	//example: {"name":"testName","email":"testEmail","notes_id": []}
	Body userEntity.User
}

// swagger:response NewUserResponse
type NewUserResponse struct {
	// in:body
	Body string
}

// swagger:route GET /user/{id} Users GetUserRequest
// Получение информации о пользователе.
// responses:
//  200: GetUserResponse

// swagger:parameters GetUserRequest
type GetUserRequest struct {
	// in:path
	// required: true
	Id string `json:"id"`
}

// swagger:response GetUserResponse
type GetUserResponse struct {
	// in:body
	Body userEntity.User
}

// swagger:route GET /notes/{id} Users ListRequest
// Список заметок пользователя.
// responses:
//  200: ListResponse

// swagger:parameters ListRequest
type ListRequest struct {
	// in:path
	// required: true
	Body string `json:"id"`
}

// swagger:response ListResponse
type ListResponse struct {
	// in:body
	List []noteEntity.Note
}
