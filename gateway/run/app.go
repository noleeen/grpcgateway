package run

import (
	"context"
	"fmt"
	"gateway/config"
	"gateway/internal/modules/controller"
	services2 "gateway/internal/modules/services"
	"gateway/internal/router"
	"gateway/pkg/responder"
	"gateway/pkg/server/serverHttp"
	"github.com/go-chi/chi"
	"golang.org/x/sync/errgroup"
	"log"
	"net/http"
	"os"
)

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() error
}

// App - структура приложения
type App struct {
	Conf   config.Config
	Server serverHttp.Serverer
	Sig    chan os.Signal
}

func NewApp(conf config.Config) *App {
	return &App{
		Conf: conf,
		Sig:  make(chan os.Signal, 1),
	}
}

// Run - запуск приложения
func (a *App) Run() error {
	ctx, cancel := context.WithCancel(context.Background())
	errGroup, ctx := errgroup.WithContext(ctx)

	// запускаем горутину для graceful shutdown
	// при получении сигнала SIGINT
	// вызываем cancel для контекста
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		log.Println("signal interrupt received", sigInt)
		cancel()
		return nil
	})

	// запускаем http сервер
	errGroup.Go(func() error {
		err := a.Server.Serve(ctx)
		if err != nil && err != http.ErrServerClosed {
			log.Fatal(err)
			return err
		}
		return nil
	})

	if err := errGroup.Wait(); err != nil {
		return err
	}

	return nil

}

func (a *App) Bootstrap(options ...interface{}) Runner {

	chiRouter := chi.NewRouter()
	resp := responder.NewResponder()

	services := services2.NewServices(a.Conf)
	controllers := controller.NewController(*services, resp)

	r := router.NewApiRouter(chiRouter, controllers)

	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", a.Conf.ServerHttp.Port),
		Handler: r,
	}

	// инициализация сервера
	a.Server = serverHttp.NewHttpServer(a.Conf, srv)

	return a
}
