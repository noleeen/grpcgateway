package config

import "os"

type ServerHttp struct {
	Port string
}

func getServerHttpConfig() ServerHttp {
	d := ServerHttp{}
	d.Port = os.Getenv("SERVER_PORT")
	return d
}
