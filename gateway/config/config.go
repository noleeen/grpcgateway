package config

type Config struct {
	ServerRPC  ServerRPC
	ServerHttp ServerHttp
}

func NewConfig() Config {
	return Config{
		ServerHttp: getServerHttpConfig(),
		ServerRPC:  getServerRPCConfig(),
	}
}
