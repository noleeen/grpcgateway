package config

import "os"

type ServerRPC struct {
	Port     string
	UserHost string
	NoteHost string
}

func getServerRPCConfig() ServerRPC {
	d := ServerRPC{}
	d.UserHost = os.Getenv("USER_RPC_HOST")
	d.NoteHost = os.Getenv("NOTE_RPC_HOST")
	d.Port = os.Getenv("RPC_PORT")

	return d
}
