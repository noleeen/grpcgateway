package noteService

import (
	"context"
	"fmt"
	"gateway/config"
	"gateway/internal/entities/noteEntity"
	pb "gitlab.com/noleeen/protogateway/gen/note"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
)

type NoteClientGRPC struct {
	client pb.NoteServiceGRPCClient
}

func NewNoteClientGRPC(conf config.Config) *NoteClientGRPC {
	//conn, err := grpc.Dial(fmt.Sprintf("%s:%s",conf.ServerRPC.NoteHost,conf.ServerRPC.Port),
	//	grpc.WithTransportCredentials(insecure.NewCredentials()))
	//if err != nil {
	//	log.Fatal("|note| grpc.Dial | error init grpc client ", err)
	//}

	conn, err := grpc.NewClient(fmt.Sprintf("%s:%s", conf.ServerRPC.NoteHost, conf.ServerRPC.Port),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatal("|note| grpc.NewClient | error init grpc client ", err)
	}

	client := pb.NewNoteServiceGRPCClient(conn)
	log.Println("grpc noteClient init")
	return &NoteClientGRPC{client: client}
}

func (n *NoteClientGRPC) GetNote(ctx context.Context, id uint32) (noteEntity.Note, error) {
	note, err := n.client.GetNote(ctx, &pb.GetNoteRequest{Id: id})
	if err != nil {
		return noteEntity.Note{}, err
	}

	return noteEntity.Note{
		Id:          note.Id,
		NoteName:    note.NoteName,
		Description: note.Description,
		UserId:      note.UserId,
	}, nil
}

func (n *NoteClientGRPC) Create(ctx context.Context, note noteEntity.Note) (uint32, error) {
	resp, err := n.client.Create(ctx, &pb.CreateRequest{
		NoteName:    note.NoteName,
		Description: note.Description,
		UserId:      note.UserId,
	})
	if err != nil {
		return 0, err
	}
	return resp.Id, nil
}

func (n *NoteClientGRPC) Delete(ctx context.Context, id uint32) error {
	_, err := n.client.Delete(ctx, &pb.DeleteRequest{Id: id})
	if err != nil {
		return err
	}
	return nil
}

func (n *NoteClientGRPC) ListByUser(ctx context.Context, userId uint32) ([]noteEntity.Note, error) {
	resp, err := n.client.ListByUser(ctx, &pb.ListByUserRequest{UserId: userId})
	if err != nil {
		return nil, err
	}

	notes := make([]noteEntity.Note, 0, len(resp.Notes))
	for _, val := range resp.Notes {
		notes = append(notes, noteEntity.Note{
			Id:          val.Id,
			NoteName:    val.NoteName,
			Description: val.Description,
			UserId:      val.UserId,
		})
	}
	return notes, nil
}
