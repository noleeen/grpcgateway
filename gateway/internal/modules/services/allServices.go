package services

import (
	"gateway/config"
	"gateway/internal/modules/services/noteService"
	"gateway/internal/modules/services/userService"
)

type Services struct {
	NoteService noteService.NoteServicer
	UserService userService.UserServicer
}

func NewServices(conf config.Config) *Services {
	nS := noteService.NewNoteClientGRPC(conf)
	uS := userService.NewUserClientGRPC(conf)

	return &Services{
		NoteService: nS,
		UserService: uS,
	}
}
