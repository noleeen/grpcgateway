package userService

import (
	"context"
	"fmt"
	"gateway/config"
	"gateway/internal/entities/userEntity"
	pb "gitlab.com/noleeen/protogateway/gen/user"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
)

type UserClientGRPC struct {
	client pb.UserServiceGRPCClient
}

func NewUserClientGRPC(conf config.Config) *UserClientGRPC {
	//conn, err := grpc.Dial(fmt.Sprintf("%s:%s",conf.ServerRPC.UserHost,conf.ServerRPC.Port),
	//	grpc.WithTransportCredentials(insecure.NewCredentials()))
	//if err != nil {
	//	log.Fatal("|user| grpc.Dial | error init grpc client ", err)
	//}

	conn, err := grpc.NewClient(fmt.Sprintf("%s:%s", conf.ServerRPC.UserHost, conf.ServerRPC.Port),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatal("|user| grpc.NewClient | error init grpc client ", err)
	}

	client := pb.NewUserServiceGRPCClient(conn)
	log.Println("grpc userClient init")
	return &UserClientGRPC{client: client}
}

func (u *UserClientGRPC) NewUser(ctx context.Context, user userEntity.User) error {

	notes := make([]int32, len(user.NotesId))

	for i := 0; i < len(user.NotesId); i++ {
		notes[i] = int32(user.NotesId[i])
	}

	_, err := u.client.Create(ctx, &pb.CreateRequest{
		Name:    user.Name,
		Email:   user.Email,
		NotesId: notes,
	})
	if err != nil {
		return err
	}
	return nil
}

func (u *UserClientGRPC) GetUser(ctx context.Context, id int) (userEntity.User, error) {
	resp, err := u.client.GetUser(ctx, &pb.GetUserRequest{Id: uint32(id)})
	if err != nil {
		return userEntity.User{}, err
	}

	notes := make([]int, len(resp.NotesId))

	for i := 0; i < len(resp.NotesId); i++ {
		notes[i] = int(resp.NotesId[i])
	}

	user := userEntity.User{
		Id:      int(resp.Id),
		Name:    resp.Name,
		Email:   resp.Email,
		NotesId: notes,
	}
	return user, nil
}

func (u *UserClientGRPC) UpdateUser(ctx context.Context, user userEntity.User) (userEntity.User, error) {
	notes_int32 := make([]int32, len(user.NotesId))

	for i := 0; i < len(user.NotesId); i++ {
		notes_int32[i] = int32(user.NotesId[i])
	}

	resp, err := u.client.Update(ctx, &pb.UpdateRequest{
		Id:      uint32(user.Id),
		Name:    user.Name,
		Email:   user.Email,
		NotesId: notes_int32,
	})
	if err != nil {
		return userEntity.User{}, err
	}

	notes := make([]int, len(resp.NotesId))

	for i := 0; i < len(resp.NotesId); i++ {
		notes[i] = int(resp.NotesId[i])
	}

	return userEntity.User{
		Id:      int(resp.Id),
		Name:    resp.Name,
		Email:   resp.Email,
		NotesId: notes,
	}, nil
}
