package controller

import (
	"encoding/json"
	"fmt"
	"gateway/internal/entities/noteEntity"
	"gateway/internal/entities/userEntity"
	"gateway/internal/modules/services"
	"gateway/pkg/responder"
	"github.com/go-chi/chi"
	"net/http"
	"strconv"
)

type Controller struct {
	services services.Services
	respond  responder.Responder
}

func NewController(services services.Services, respond responder.Responder) *Controller {
	return &Controller{
		services: services,
		respond:  respond,
	}
}

func (c *Controller) NewUser(w http.ResponseWriter, r *http.Request) {
	var user userEntity.User
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		c.respond.ErrorBadRequest(w, err)
		return
	}
	err := c.services.UserService.NewUser(r.Context(), user)
	if err != nil {
		c.respond.ErrorInternal(w, err)
		return
	}

	c.respond.OutputJSON(w, fmt.Sprintf("user with name: %s created successfuly", user.Name))
}

func (c *Controller) GetUser(w http.ResponseWriter, r *http.Request) {
	//var id int
	//if err := json.NewDecoder(r.Body).Decode(&id); err != nil {
	//	c.respond.ErrorBadRequest(w, err)
	//	return
	//}
	id := chi.URLParam(r, "id")
	userIdInt, err := strconv.Atoi(id)
	if err != nil {
		c.respond.ErrorBadRequest(w, err)
		return
	}

	user, err := c.services.UserService.GetUser(r.Context(), userIdInt)
	if err != nil {
		c.respond.ErrorInternal(w, err)
		return
	}

	c.respond.OutputJSON(w, user)
}

func (c *Controller) ListNotesByUser(w http.ResponseWriter, r *http.Request) {
	//var userId uint32
	//if err := json.NewDecoder(r.Body).Decode(&userId); err != nil {
	//	c.respond.ErrorBadRequest(w, err)
	//	return
	//}

	id := chi.URLParam(r, "id")
	userIdInt, err := strconv.Atoi(id)
	if err != nil {
		c.respond.ErrorBadRequest(w, err)
		return
	}

	notes, err := c.services.NoteService.ListByUser(r.Context(), uint32(userIdInt))
	if err != nil {
		c.respond.ErrorInternal(w, err)
		return
	}

	c.respond.OutputJSON(w, notes)
}

func (c *Controller) NewNote(w http.ResponseWriter, r *http.Request) {
	var note noteEntity.Note
	if err := json.NewDecoder(r.Body).Decode(&note); err != nil {
		c.respond.ErrorBadRequest(w, err)
		return
	}
	noteId, err := c.services.NoteService.Create(r.Context(), note)
	if err != nil {
		c.respond.ErrorBadRequest(w, err)
		return
	}

	user, err := c.services.UserService.GetUser(r.Context(), int(note.UserId))
	if err != nil {

		c.respond.ErrorInternal(w, err)
		return
	}

	user.NotesId = append(user.NotesId, int(noteId))
	_, err = c.services.UserService.UpdateUser(r.Context(), user)
	if err != nil {
		c.respond.ErrorInternal(w, err)
		return
	}

	c.respond.OutputJSON(w, fmt.Sprintf(
		"new note %s created successfuly and add to user %s with id:%d",
		note.NoteName, user.Name, user.Id))
}

func (c *Controller) DeleteNote(w http.ResponseWriter, r *http.Request) {
	//var noteId uint32
	//if err := json.NewDecoder(r.Body).Decode(&noteId); err != nil {
	//	c.respond.ErrorBadRequest(w, err)
	//	return
	//}

	id := chi.URLParam(r, "id")
	noteId, err := strconv.Atoi(id)

	note, err := c.services.NoteService.GetNote(r.Context(), uint32(noteId))
	if err != nil {
		c.respond.ErrorInternal(w, err)
		return
	}

	err = c.services.NoteService.Delete(r.Context(), uint32(noteId))
	if err != nil {
		c.respond.ErrorInternal(w, err)
		return
	}

	user, err := c.services.UserService.GetUser(r.Context(), int(note.UserId))
	if err != nil {
		c.respond.ErrorInternal(w, err)
		return
	}

	for i, val := range user.NotesId {
		if int(noteId) == val {
			user.NotesId = append(user.NotesId[:i], user.NotesId[i+1:]...)
			break
		}
	}

	_, err = c.services.UserService.UpdateUser(r.Context(), user)
	if err != nil {
		c.respond.ErrorInternal(w, err)
		return
	}

	c.respond.OutputJSON(w, fmt.Sprintf(
		"note %s deleted successfuly and update user %s with id: %d",
		note.NoteName, user.Name, user.Id))

}
