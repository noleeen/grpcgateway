package router

import (
	"gateway/internal/modules/controller"
	"gateway/static"
	"github.com/go-chi/chi"
	"net/http"
)

func NewApiRouter(r *chi.Mux, controller *controller.Controller) http.Handler {

	r.Get("/swagger", static.SwaggerUI)
	r.Get("/static/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))).ServeHTTP(w, r)
	})

	r.Post("/user", controller.NewUser)
	r.Get("/user/{id}", controller.GetUser)
	r.Get("/notes/{id}", controller.ListNotesByUser)
	r.Post("/note", controller.NewNote)
	r.Delete("/note/{id}", controller.DeleteNote)

	return r
}
