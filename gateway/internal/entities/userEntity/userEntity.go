package userEntity

type User struct {
	Id      int    `json:"id"`
	Name    string `json:"name"`
	Email   string `json:"email"`
	NotesId []int  `json:"notes_id"`
}
