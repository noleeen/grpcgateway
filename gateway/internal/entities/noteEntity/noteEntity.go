package noteEntity

type Note struct {
	Id          uint32 `json:"id"`
	NoteName    string `json:"note_name"`
	Description string `json:"description"`
	UserId      uint32 `json:"user_id"`
}
