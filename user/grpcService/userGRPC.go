package grpcService

import (
	"context"
	pb "gitlab.com/noleeen/protogateway/gen/user"
	"user/internal/modules/userService"
	"user/internal/userEntity"
)

type UserGRPC struct {
	service userService.UserServicer
	pb.UnimplementedUserServiceGRPCServer
}

func NewUserGRPC(service userService.UserServicer) *UserGRPC {
	return &UserGRPC{service: service}
}

func (u *UserGRPC) Create(ctx context.Context, in *pb.CreateRequest) (*pb.CreateResponse, error) {
	notes := make([]int, len(in.NotesId))
	for i := 0; i < len(in.NotesId); i++ {
		notes[i] = int(in.NotesId[i])
	}

	err := u.service.NewUser(ctx, userEntity.User{
		Name:    in.Name,
		Email:   in.Email,
		NotesId: notes,
	})
	if err != nil {
		return &pb.CreateResponse{Success: false}, err
	}
	return &pb.CreateResponse{Success: true}, nil
}

func (u *UserGRPC) GetUser(ctx context.Context, in *pb.GetUserRequest) (*pb.GetUserResponse, error) {

	user, err := u.service.GetUser(ctx, int(in.Id))
	if err != nil {
		return nil, err
	}

	notes := make([]int32, len(user.NotesId))
	for i := 0; i < len(user.NotesId); i++ {
		notes[i] = int32(user.NotesId[i])
	}

	userProto := &pb.GetUserResponse{
		Id:      uint32(user.Id),
		Name:    user.Name,
		Email:   user.Email,
		NotesId: notes,
	}

	return userProto, nil
}

func (u *UserGRPC) Update(ctx context.Context, in *pb.UpdateRequest) (*pb.UpdateResponse, error) {
	notes_int := make([]int, len(in.NotesId))
	for i := 0; i < len(in.NotesId); i++ {
		notes_int[i] = int(in.NotesId[i])
	}

	user, err := u.service.UpdateUser(ctx, userEntity.User{
		Id:      int(in.Id),
		Name:    in.Name,
		Email:   in.Email,
		NotesId: notes_int,
	})
	if err != nil {
		return nil, err
	}

	notes_int32 := make([]int32, len(user.NotesId))
	for i := 0; i < len(user.NotesId); i++ {
		notes_int32[i] = int32(user.NotesId[i])
	}

	userProto := &pb.UpdateResponse{
		Id:      uint32(user.Id),
		Name:    user.Name,
		Email:   user.Email,
		NotesId: notes_int32,
	}

	return userProto, nil
}
