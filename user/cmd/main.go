package main

import (
	"log"
	"os"
	"user/config"
	"user/run"
)

func main() {

	conf := config.NewConfig()

	app := run.NewApp(conf)

	if err := app.Bootstrap().Run(); err != nil {
		log.Println("|main_User| app run error:", err)
		os.Exit(2)
	}
}
