package userStorage

import (
	"context"
	"user/internal/userEntity"
)

type UserStorager interface {
	CreateUser(ctx context.Context, user userEntity.User) error
	GetUserById(ctx context.Context, id int) (userEntity.User, error)
	UpdateUser(ctx context.Context, user userEntity.User) (userEntity.User, error)
}
