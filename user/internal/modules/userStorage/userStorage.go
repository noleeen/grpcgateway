package userStorage

import (
	"context"
	"database/sql"
	"github.com/lib/pq"
	"log"
	"user/internal/userEntity"
)

type UserStorage struct {
	db *sql.DB
}

func NewUserStorage(db *sql.DB) UserStorager {
	return &UserStorage{db: db}
}

func (u *UserStorage) CreateUser(ctx context.Context, user userEntity.User) error {
	q := "INSERT INTO users(name,email,notes_id)values($1,$2,$3)"
	_, err := u.db.ExecContext(ctx, q, user.Name, user.Email, pq.Array(user.NotesId))
	if err != nil {
		log.Println("error CreateUser:", err)
		return err
	}
	return nil
}

func (u *UserStorage) GetUserById(ctx context.Context, id int) (userEntity.User, error) {
	q := "SELECT id, name, email, notes_id FROM users WHERE id=$1"
	var user userEntity.User

	var x []sql.NullInt64
	err := u.db.QueryRowContext(ctx, q, id).Scan(&user.Id, &user.Name, &user.Email, pq.Array(&x))
	if err != nil {
		if err == sql.ErrNoRows {
			// Обработка случая, когда нет результатов для заданного id
			log.Println("No user found with id:", id)
			return userEntity.User{}, err
		}
		log.Println("error GetUserById:", err)
		return userEntity.User{}, err
	}

	for _, val := range x {
		user.NotesId = append(user.NotesId, int(val.Int64))
	}

	return user, nil
}

func (u *UserStorage) UpdateUser(ctx context.Context, user userEntity.User) (userEntity.User, error) {

	q := "UPDATE users SET id=$4, name=$1, notes_id=$2 WHERE email=$3"
	_, err := u.db.ExecContext(ctx, q, user.Name, pq.Array(user.NotesId), user.Email, user.Id)
	if err != nil {
		log.Println("error UpdateUser:", err)
		return userEntity.User{}, err
	}
	return u.GetUserById(ctx, user.Id)
}
