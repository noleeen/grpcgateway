package userService

import (
	"context"
	"user/internal/userEntity"
)

type UserServicer interface {
	NewUser(ctx context.Context, user userEntity.User) error
	GetUser(ctx context.Context, id int) (userEntity.User, error)
	UpdateUser(ctx context.Context, user userEntity.User) (userEntity.User, error)
}
