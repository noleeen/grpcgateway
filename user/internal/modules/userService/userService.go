package userService

import (
	"context"
	"database/sql"
	"user/internal/modules/userStorage"
	"user/internal/userEntity"
)

type UserService struct {
	storage userStorage.UserStorager
}

func NewUserService(db *sql.DB) UserServicer {
	return &UserService{storage: userStorage.NewUserStorage(db)}
}

func (uS *UserService) NewUser(ctx context.Context, user userEntity.User) error {
	return uS.storage.CreateUser(ctx, user)
}

func (uS *UserService) GetUser(ctx context.Context, id int) (userEntity.User, error) {
	return uS.storage.GetUserById(ctx, id)
}

func (uS *UserService) UpdateUser(ctx context.Context, user userEntity.User) (userEntity.User, error) {
	return uS.storage.UpdateUser(ctx, user)
}
