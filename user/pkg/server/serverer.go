package server

import "context"

type Serverer interface {
	Serve(ctx context.Context) error
}
