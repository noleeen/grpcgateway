package grpcService

import (
	"context"
	pb "gitlab.com/noleeen/protogateway/gen/note"
	"note/internal/modules/noteService"
	"note/internal/noteEntity"
)

type NoteGRPC struct {
	service noteService.NoteServicer
	pb.UnimplementedNoteServiceGRPCServer
}

func NewNoteGRPC(service noteService.NoteServicer) *NoteGRPC {
	return &NoteGRPC{service: service}
}

func (n *NoteGRPC) GetNote(ctx context.Context, in *pb.GetNoteRequest) (*pb.GetNoteResponse, error) {
	note, err := n.service.GetNote(ctx, in.Id)
	if err != nil {
		return nil, err
	}

	return &pb.GetNoteResponse{
		Id:          note.Id,
		NoteName:    note.NoteName,
		Description: note.Description,
		UserId:      note.UserId,
	}, nil
}

func (n *NoteGRPC) Create(ctx context.Context, in *pb.CreateRequest) (*pb.CreateResponse, error) {
	id, err := n.service.Create(ctx, noteEntity.Note{
		NoteName:    in.NoteName,
		Description: in.Description,
		UserId:      in.UserId,
	})
	if err != nil {
		return &pb.CreateResponse{Success: false}, err
	}

	return &pb.CreateResponse{
		Id:      id,
		Success: true,
	}, nil
}

func (n *NoteGRPC) Delete(ctx context.Context, in *pb.DeleteRequest) (*pb.DeleteResponse, error) {
	err := n.service.Delete(ctx, in.Id)
	if err != nil {
		return &pb.DeleteResponse{Success: false}, err
	}
	return &pb.DeleteResponse{Success: true}, nil
}

func (n *NoteGRPC) ListByUser(ctx context.Context, in *pb.ListByUserRequest) (*pb.ListByUserResponse, error) {
	notes, err := n.service.ListByUser(ctx, in.UserId)
	if err != nil {
		return nil, err
	}
	var notesProto []*pb.GetNoteResponse

	for _, val := range notes {
		notesProto = append(notesProto, &pb.GetNoteResponse{
			Id:          val.Id,
			NoteName:    val.NoteName,
			Description: val.Description,
			UserId:      val.UserId,
		})
	}
	return &pb.ListByUserResponse{Notes: notesProto}, nil
}
