CREATE TABLE IF NOT EXISTS notes(
    id SERIAL PRIMARY KEY,
    note_name VARCHAR(255) NOT NULL,
    description TEXT,
    user_id INTEGER NOT NULL
);