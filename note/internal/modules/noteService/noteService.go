package noteService

import (
	"context"
	"database/sql"
	"note/internal/modules/noteStorage"
	"note/internal/noteEntity"
)

type NoteService struct {
	storage noteStorage.NoteStorager
}

func NewNoteService(db *sql.DB) NoteServicer {
	return &NoteService{storage: noteStorage.NewNoteStorage(db)}
}

func (n *NoteService) GetNote(ctx context.Context, id uint32) (noteEntity.Note, error) {
	return n.storage.GetById(ctx, id)
}

func (n *NoteService) Create(ctx context.Context, note noteEntity.Note) (uint32, error) {
	return n.storage.Create(ctx, note)
}

func (n *NoteService) Delete(ctx context.Context, id uint32) error {
	return n.storage.Delete(ctx, id)
}

func (n *NoteService) ListByUser(ctx context.Context, userId uint32) ([]noteEntity.Note, error) {
	return n.storage.ListByUser(ctx, userId)
}
