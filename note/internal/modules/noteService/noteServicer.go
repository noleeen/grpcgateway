package noteService

import (
	"context"
	"note/internal/noteEntity"
)

type NoteServicer interface {
	GetNote(ctx context.Context, id uint32) (noteEntity.Note, error)
	Create(ctx context.Context, note noteEntity.Note) (uint32, error)
	Delete(ctx context.Context, id uint32) error
	ListByUser(ctx context.Context, userId uint32) ([]noteEntity.Note, error)
}
