package noteStorage

import (
	"context"
	"database/sql"
	"log"
	"note/internal/noteEntity"
)

type NoteStorage struct {
	db *sql.DB
}

func NewNoteStorage(db *sql.DB) NoteStorager {
	return &NoteStorage{db: db}
}

func (n *NoteStorage) GetById(ctx context.Context, id uint32) (noteEntity.Note, error) {
	var note noteEntity.Note

	q := "SELECT id, note_name, description, user_id FROM notes WHERE id = $1"
	err := n.db.QueryRowContext(ctx, q, id).
		Scan(&note.Id, &note.NoteName, &note.Description, &note.UserId)
	if err != nil {
		log.Println("error |note| GetById:", err)
		return noteEntity.Note{}, err
	}
	return note, nil
}

func (n *NoteStorage) Create(ctx context.Context, note noteEntity.Note) (uint32, error) {
	q := "INSERT INTO notes (note_name, description,user_id) VALUES ($1,$2,$3) RETURNING id"

	var id uint32
	err := n.db.QueryRowContext(ctx, q, note.NoteName, note.Description, note.UserId).Scan(&id)
	if err != nil {
		log.Println("error |note| Create:", err)
		return 0, err
	}
	return id, nil
}

func (n *NoteStorage) Delete(ctx context.Context, id uint32) error {

	q := "DELETE FROM notes WHERE id = $1"
	_, err := n.db.ExecContext(ctx, q, id)
	if err != nil {
		log.Println("error |note| Delete:", err)
		return err
	}

	return nil
}

func (n *NoteStorage) ListByUser(ctx context.Context, userId uint32) ([]noteEntity.Note, error) {
	var notes []noteEntity.Note

	q := "SELECT id, note_name, description, user_id FROM notes WHERE user_id = $1"
	rows, err := n.db.QueryContext(ctx, q, int(userId))
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var note noteEntity.Note
		err1 := rows.Scan(&note.Id, &note.NoteName, &note.Description, &note.UserId)
		if err1 != nil {
			return nil, err1
		}
		notes = append(notes, note)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return notes, nil
}
