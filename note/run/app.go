package run

import (
	"context"
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	pb "gitlab.com/noleeen/protogateway/gen/note"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"log"
	"net/http"
	"note/config"
	"note/grpcService"
	"note/internal/modules/noteService"
	"note/pkg/server"
	"os"
)

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() error
}

// App - структура приложения
type App struct {
	Conf config.Config
	Rpc  server.Serverer
	Sig  chan os.Signal
}

func NewApp(conf config.Config) *App {
	return &App{
		Conf: conf,
		Sig:  make(chan os.Signal, 1),
	}
}

// Run - запуск приложения
func (a *App) Run() error {
	ctx, cancel := context.WithCancel(context.Background())
	errGroup, ctx := errgroup.WithContext(ctx)

	// запускаем горутину для graceful shutdown
	// при получении сигнала SIGINT
	// вызываем cancel для контекста
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		log.Println("signal interrupt received", sigInt)
		cancel()
		return nil
	})

	// запускаем RPC сервер
	errGroup.Go(func() error {
		err := a.Rpc.Serve(ctx)
		if err != nil && err != http.ErrServerClosed {
			log.Fatal(err)
			return err
		}
		return nil
	})

	if err := errGroup.Wait(); err != nil {
		return err
	}

	return nil

}

func (a *App) Bootstrap(options ...interface{}) Runner {

	//postgresql
	dsn := a.Conf.GetDsnDB()
	fmt.Println("dsn:", dsn)
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		fmt.Println("error sql.Open")
		log.Fatal(err)
	}

	//ctx := context.Background()

	noteService2 := noteService.NewNoteService(db)

	serverGRPC := grpc.NewServer()

	a.Rpc = server.NewServerRPC(a.Conf.ServerRPC, serverGRPC)
	noteGRPC := grpcService.NewNoteGRPC(noteService2)
	pb.RegisterNoteServiceGRPCServer(serverGRPC, noteGRPC)

	return a
}
